import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { LoginComponent } from '../login/login.component';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private matdialog : MatDialog) { }

  ngOnInit() {
  }

  openLoginForm(){
      this.matdialog.open(LoginComponent, {width: '500px', height: '450px'});

  }

}

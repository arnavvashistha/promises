  class Comment {
    rating: number;
    comment: string;
    author: string;
    date: string;
}
export {Comment};
